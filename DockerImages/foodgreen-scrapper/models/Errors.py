
class RecipeException( Exception ):
    """ Recipe Exception """
    
    _message = None

    def __init__( self ):
        """ Default constructor """
        self._message = None

    @property
    def message( self ) -> str:
        """ getter on _message """
        return self._message
    
    @message.setter
    def message( self, message ) -> None:
        """ setter on _message """
        self._message = message

