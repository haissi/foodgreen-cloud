class Step( object ):
    def __init__( self, text, stepNumber, recipeID ):
        self._text       = text
        self._stepNumber = stepNumber
        self._recipeID   = recipeID

    @property
    def text( self ) -> str:
        """ getting intance text """
        return self._text
    
    @text.setter
    def text( self, text: str ) -> None:
        """ setting text """
        self._text = text

    @property
    def stepNumber( self ) -> int:
        """ getting stepNumber """
        return self._stepNumber

    @stepNumber.setter
    def stepNumber( self, stepNumber ) -> None:
        """ set stepNumber """
        self._stepNumber = stepNumber

    @property
    def recipeID( self ) -> int:
        """ getting ID """
        return self._recipeID
    
    @recipeID.setter
    def recipeID( self, recipeID ) -> None:
        """ set recipeID """
        self._recipeID = recipeID
