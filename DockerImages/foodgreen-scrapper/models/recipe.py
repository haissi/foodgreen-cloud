class Recipe( object ):

    def __init__( self, title: str, level: float, cookTime: int, preparationTime: int, nbPeople: int, imgPath: str ):
        self._id              = None
        self._title           = title
        self._level           = level 
        self._cookTime        = cookTime
        self._preparationTime = preparationTime
        self._nbPeople        = nbPeople
        self._imgPath         = imgPath

    @property
    def id( self ) -> int:
        """ getting recipe id """
        return self._id

    @id.setter
    def id( self, id ) -> None:
        """ setting id """
        self._id = id

    @property
    def title( self ) -> str:
        """ get title """
        return self._title

    @title.setter
    def title( self, title ) -> None:
        """ set title """
        self._title = title
    
    @property
    def level( self ):
        """ get level """
        return self._level

    @level.setter
    def level( self, level ) -> None:
        """ set level """
        self._level = level
    
    @property
    def cookTime( self ) -> int:
        """ get cookTime """
        return self._cookTime

    @cookTime.setter
    def cookTime( self, cookTime ) -> None:
        """ set cookTime """
        self._cookTime = cookTime
    
    @property
    def preparationTime( self ) -> int:
        """ get preparationTime """
        return self._preparationTime

    @preparationTime.setter
    def preparationTime( self, preparationTime ) -> None:
        """ set preparationTime """
        self._preparationTime = preparationTime
    
    @property
    def nbPeople( self ) -> int:
        """ get nbPeople """
        return self._nbPeople

    @nbPeople.setter
    def nbPeople( self, nbPeople ) -> None:
        """ set nbPeople """
        self._nbPeople = nbPeople

    @property
    def imgPath( self ) -> str:
        """ get img path """
        return self._imgPath
    
    @imgPath.setter
    def imgPath( self, imgPath ) -> None:
        """ setter imgPath """
        self._imgPath = imgPath
    
    
    

