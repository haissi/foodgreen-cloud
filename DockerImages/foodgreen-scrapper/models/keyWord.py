class KeyWord ( object ):
    def __init__( self, word ):
        self._id = None
        self._word = word
    @property
    def id( self ) -> int:
        """ getting keyWord ID """
        return self._id

    @id.setter
    def id( self, id ) -> None:
        """ setting keyWord ID """
        self._id = id

    @property
    def word( self ) -> str:
        """ getting keyWord word """
        return self._word
    
    @word.setter
    def word( self, word ) -> None:
        """ setting keyWord word """
        self._word = word