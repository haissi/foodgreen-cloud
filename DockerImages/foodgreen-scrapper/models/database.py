#!/usr/bin/python
import psycopg2
from .config import config
from models.step import Step
from models.color import Color
from models.recipe import Recipe
from models.keyWord import KeyWord
from models.measure import Measure
from models.ingredient import Ingredient
from models.Errors import RecipeException
from sqlalchemy.exc import IntegrityError

class DataBase( object ):

    _cursor = None
    _connection = None

    def __init__( self ):
        """ Class Constructor """
        self.connect()

    def __del__( self ):
        """ Class Destructor """
        if( self._connection is not None ):
            self._connection.close()
        
        if( self._cursor is not None ):
            self._cursor.close()
        
        print('Database connection closed.')

    @property
    def cursor( self ):
        """ getting cursor """
        return self._connection.cursor()

    @classmethod
    def connect( self ):
        """ Create the database connection """
        self._connection = None
        try:
            params = config()
            # connect to the PostgreSQL server
            print('Connecting to the PostgreSQL database...')
            self._connection = psycopg2.connect( **params )

        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            exit()
    
    @classmethod
    def insertRecipe( self, recipe: Recipe ):
        """ Insert Recipe in DB """
        recipeSelectIDQuery  = 'SELECT id FROM Recipe r where r.title = %s '
        imageInsertionQuery  = 'INSERT INTO image_recipe ( "url", "recipeId" ) VALUES ( %s, %s )'
        recipeInsertionQuery = 'INSERT INTO Recipe ( "title", "level", "cook_time", "preparation_time", "number_people" ) VALUES ( %s, %s, %s, %s, %s ) RETURNING id;'
        if( self._connection is None ):
            raise psycopg2.DatabaseError()
        
        try:
            # Cursor creation
            self._cursor = self._connection.cursor()
            self._cursor.execute( recipeInsertionQuery, ( recipe.title, recipe.level, recipe.cookTime, recipe.preparationTime, recipe.nbPeople ))
            # Getting the current recipe ID
            recipe.id = self._cursor.fetchone()[0]
            self._cursor.execute( imageInsertionQuery, ( recipe.imgPath, recipe.id ))
        except ( Exception, psycopg2.IntegrityError ) as e:
            if( e.pgcode == '23505' ):
                print( Color.RED.value + "[-] ", end=Color.END.value )
                print( Color.BLUE.value + "Recipe" + Color.YELLOW.value +" '{}'".format( recipe.title ) + Color.GREEN.value + " already exist.", end= Color.END.value + '\n')
                raise RecipeException();
        finally:
            self._connection.commit()
            self._cursor.close()


    @classmethod
    def insertStep( self, steps: Step, recipeID: int ):
        """ Insert Recipe in DB """
        stepInsertionQuery = 'INSERT INTO step_recipe ( "step", "number_step", "recipeId" ) VALUES ( %s, %s, %s )'
        if( self._connection is None ):
            raise psycopg2.DatabaseError()

        self._cursor = self._connection.cursor()
        for step in steps:
            try:
                self._cursor.execute( stepInsertionQuery, ( step.text, step.stepNumber, recipeID ))
            except( Exception, psycopg2.IntegrityError ) as e:
                if( e.pgcode == '23505' ):
                    print( Color.RED.value + "[-] ", end=Color.END.value )
                    print( Color.BLUE.value + "Step " + Color.YELLOW.value +" '{}'".format( step.text ) + Color.GREEN.value + " already exist.", end= Color.END.value + '\n')
            finally:
                self._connection.commit()

        self._cursor.close()

    @classmethod
    def insertKeyWords( self, keyWords: KeyWord, recipeID: int ):
        """ Insert Recipe in DB """
        keyWordInsertionQuery = 'INSERT INTO keyword_recipe ( value ) VALUES ( %s ) RETURNING id;'
        recipeKeyWordInsertionQuery = 'INSERT INTO recipe_keyword_recipe ( "recipeId", "keywordRecipeId" ) VALUES ( %s, %s )'

        if( self._connection is None ):
            raise psycopg2.DatabaseError()

        self._cursor = self._connection.cursor()
        for keyWord in keyWords:
            try:
                # Insert on Keyword table
                self._cursor.execute( keyWordInsertionQuery, ( keyWord.word, ))
                keyWord.id = self._cursor.fetchone()[0]
                # Insert on liaison table
                self._cursor.execute( recipeKeyWordInsertionQuery, ( recipeID ,keyWord.id ))
            except( Exception, psycopg2.IntegrityError ) as e:
                if( e.pgcode == '23505' ):
                    print( Color.RED.value + "[-] ", end=Color.END.value )
                    print( Color.BLUE.value + "Keyword " + Color.YELLOW.value +"'{}'".format( keyWord.word ) + Color.GREEN.value + " already exist.", end= Color.END.value + '\n')
            finally:
                self._connection.commit()

        self._cursor.close()

    @classmethod
    def insertIngredient( self, ingredients: Ingredient, recipeID: int ):
        """ Insert Recipe in DB """
        ingredientInsertionQuery = 'INSERT INTO ingredient ( "name" ) VALUES ( %s ) RETURNING id;'
        ingredientRecipeInsertionQuery = 'INSERT INTO ingredient_recipe ( "quantity", "measures", "ingredientId", "recipeId" ) VALUES ( %s, %s, %s, %s ) RETURNING id;'
        if( self._connection is None ):
            raise psycopg2.DatabaseError()

        self._cursor = self._connection.cursor()
        for ingredient in ingredients:
            try:
                # Insert Ingredient
                self._cursor.execute( ingredientInsertionQuery, ( ingredient.name, ))
                ingredient.id = self._cursor.fetchone()[0]
                # Insert Ingredient Recipe
                self._cursor.execute( ingredientRecipeInsertionQuery, ( ingredient.quantity, ingredient.measures, ingredient.id, recipeID, ))
            except( Exception, psycopg2.IntegrityError ) as e:
                if( e.pgcode == '23505' ):
                    print( Color.RED.value + "[-] ", end=Color.END.value )
                    print( Color.BLUE.value + "Ingredient" + Color.YELLOW.value +" '{}'".format( ingredient.name ) + Color.GREEN.value + " already exist.", end= Color.END.value + '\n')
            finally:
                self._connection.commit()
        
        self._cursor.close()
