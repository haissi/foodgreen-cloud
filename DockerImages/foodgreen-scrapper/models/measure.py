from enum import Enum
class Measure( Enum ):
    G    = "g"
    KG   = "kg"
    L    = "l"
    CL   = "cl"
    ML   = "ml"
    FULL = ""
    