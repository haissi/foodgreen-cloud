class Formater( object ):
    """ Formater class who will format all datas """

    @staticmethod
    def FormatTime( texteFromDOM: str ):
        """ return the formated date """
        texteFromDOM = texteFromDOM.strip( "\t\n" )
        # If Time is explain in minute
        if( 'min' in texteFromDOM and 'h' not in texteFromDOM ):
            cook_time = texteFromDOM.strip( "abcdefghijklmnopqrstuvwxyz" ).strip()
            if( ' ' in cook_time ):
                cook_time = cook_time.replace( ' ', '' )
            if( 'min' in cook_time ):
                cook_time = cook_time.split( 'min' )
                cook_time = cook_time[0]
            
        # If Time is explain in hour
        elif( 'min' not in texteFromDOM and 'h' in texteFromDOM ):
            time = texteFromDOM.strip( "abcdefghijklmnopqrstuvwxyz" ).strip()
            if( 'h' in time ):
                time = time.replace( 'h', '' )
            cook_time = int( time ) * 60
        
        return cook_time