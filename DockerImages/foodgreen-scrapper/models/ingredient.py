
class Ingredient ( object ):
    def __init__( self, name, quantity, measures ):
        self._id = None
        self._name = name
        self._quantity = quantity
        self._measures = None

    @property
    def id( self ) -> int:
        """ getting ingredient ID """
        return self._id

    @id.setter
    def id( self, id ) -> None:
        """ setting ingredient ID """
        self._id = id

    @property
    def name( self ) -> str:
        """ getting ingredient name """
        return self._name

    @name.setter
    def name( self, name ) -> None:
        """ setting ingredient name"""
        self._name = name

    @property
    def quantity( self ) -> str:
        """ getting ingredient quantity """
        return self._quantity

    @quantity.setter
    def quantity( self, quantity ) -> None:
        """ setting ingredient quantity"""
        self._quantity = quantity
    
    @property
    def measures( self ):
        """ getting ingredient measures """
        return self._measures

    @measures.setter
    def measures( self, measures ) -> None:
        """ setting ingredient measures """
        self._measures = measures