#!/usr/bin/python
from .step import Step
from .recipe import Recipe
from .keyWord import KeyWord
from .ingredient import Ingredient

class DataModel( object ):
    def __init__( self, recipe, ingredients, keyWords, steps ):
        self._recipe      = recipe
        self._ingredients = ingredients
        self._keyWords    = keyWords
        self._steps       = steps

    
    @property
    def recipe( self ) -> Recipe:
        """ getting Recipe """
        return self._recipe

    @recipe.setter
    def recipe( self, recipe ) -> None:
        """ setting Recipe """
        self._recipe = recipe

    @property
    def ingredients( self ) -> Ingredient:
        """ getting Ingredients """
        return self._ingredients

    @ingredients.setter
    def ingredients( self, ingredients ) -> None:
        """ setting Ingredients """
        self._ingredients = ingredients

    @property
    def keyWords( self ) -> KeyWord:
        """ getting keyWords """
        return self._keyWords
    
    @keyWords.setter
    def keyWords( self, keyWords ) -> None:
        """ setting keyWords """
        self._keyWords = keyWords
    
    @property
    def steps( self ) -> Step:
        """ getting Step """
        return self._steps

    @steps.setter
    def steps( self, steps ) -> None:
        """ setting Step """
        self._steps = steps