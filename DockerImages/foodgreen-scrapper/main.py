import json
import time
import urllib
import urllib3
import requests
from scrapy import Selector
from models.step import Step
from models.color import Color
from models.recipe import Recipe
from models.keyWord import KeyWord
from models.measure import Measure
from models.formater import Formater
from models.database import DataBase
from configparser import ConfigParser
from models.dataModel import DataModel
from models.ingredient import Ingredient
from models.Errors import RecipeException

REQUEST_HEADER = {
    'user-agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36',
    'accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'accept-language':'fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7',
    'Cache-Control':'no-cache'
}

APP_SESSION = requests.Session()

def ingredientScrapper( dom ):
    """ Scrapping ingredient informations """
    ingredientArray: Ingredient = []
    dirtyIngredient = dom.css( "ul.recipe-ingredients__list > li" )
    for ingredient in dirtyIngredient:
        name = ingredient.css( "span.ingredient::text" ).extract_first().strip()
        quantity = ingredient.css( "span.recipe-ingredient-qt::text" ).extract_first()
        
        if( '/' in str( quantity ) ):
            quantity = int( quantity.split('/')[1] ) / int( quantity.split('/')[0] )
        elif( quantity == None ):
            quantity = -1
        measure: Measure = None

        if( 'kg ' in name ):
            measure = Measure.KG
            name = name.replace( measure.value + ' de ', '' )
        elif( 'g ' in name ):
            measure = Measure.G
            name = name.replace( measure.value + ' de ', '' )
        elif( 'l ' in name or 'L ' in name ):
            measure = Measure.L
            name = name.replace( measure.value + ' de ', '' )
        else:
            measure = Measure.FULL
        # Adding the ingredient to the ingredients Array
        ingredientArray.append( Ingredient( name, quantity, measure ) )

    return ingredientArray

def keywordsScrapper( dom ):
    """ Scrapping keywords informations """
    keyWordArray: KeyWord = []
    words = dom.css( "ul.mrtn-tags-list > li.mrtn-tag" )
    
    for word in words:
        if( word.css( "a.mrtn-tag--grey::text" ).get() == None ):
            word = word.css( "li.mrtn-tag::text" ).get()
        else:
            word = word.css( "a.mrtn-tag--grey::text" ).get()
        
        if( 'Préparation : ' not in word and 'Cuisson' not in word and 'Repos' not in word  ):
            word = word.strip( "\t\n" )
            keyWordArray.append( KeyWord( word ))

    return keyWordArray


def stepsScrapper( dom, recipeID ):
    """ Scrapping steps informations """
    steps: Step = []
    dirtySteps = dom.css( "li.recipe-preparation__list__item::text" ).getall()
    for i, step in enumerate( dirtySteps, start=1 ):
        step = step.strip( "\n\t" ).strip()
        steps.append( Step( step, i, recipeID ))

    return steps

def recipeScrapper( dom ):
    """ Scrapping recipe informations """
    main_title = dom.css( "h1.main-title::text" ).extract_first()
    
    rate_level = dom.css( "span.recipe-infos-users__rating::text" ).extract_first()
    rate_level = rate_level.split( "/" )[0]
    
    imgPath = dom.css( "a.af-pin-it" ).xpath( '@href' ).extract_first()
    imgPath = imgPath.split( "media=" )[1]
    imgPath = urllib.parse.unquote( imgPath )

    # If there is a cookTime
    cook_time = -1 if dom.css( "div.recipe-infos__timmings__cooking > span::text" ) \
                == \
                [] else dom.css( "div.recipe-infos__timmings__cooking > span::text" ).extract()[0]
    if( cook_time != -1 ):
        cook_time = Formater.FormatTime( texteFromDOM=cook_time )

    
    preparationTime = dom.css( "div.recipe-infos__timmings__preparation::text" )
    if( preparationTime != [] ):
        preparationTime = dom.css( "div.recipe-infos__timmings__preparation::text" ).extract()[1]
        preparationTime = Formater.FormatTime( texteFromDOM=preparationTime )
    else:
        preparationTime = -1
    
    nbPeople = dom.xpath('//span[@class="title-2 recipe-infos__quantity__value"]/text()').get()
    if( nbPeople == None ):
        nbPeople = -1

    return Recipe( str( main_title ), float( rate_level ), int( cook_time ), int( preparationTime ), int( nbPeople ), str( imgPath ) )


def DOMParser( dom ):
    """ Parse dom """
    
    recipe:      Recipe     = recipeScrapper( dom )
    steps:       Step       = stepsScrapper( dom, recipe.id )
    keywords:    KeyWord    = keywordsScrapper( dom )
    ingredients: Ingredient = ingredientScrapper( dom )

    dbConnect: DataBase = DataBase()

    
    try:
        dbConnect.insertRecipe( recipe )
        dbConnect.insertStep( steps, recipe.id )
        dbConnect.insertKeyWords( keywords, recipe.id )
        dbConnect.insertIngredient( ingredients, recipe.id )
    except ( Exception, RecipeException ) as e:
        return e
    finally:
        del dbConnect

def urlAppender( url, urls ):
    """ Append URL to table """
    for u in url:
        if( 'https' in u and 'www.marmiton.org' in u ):
            urls.append( u )
        else:
            urls.append( 'https://www.marmiton.org' + u )

def extractURIFromPage( URI, recipeUrls ):
    """ extract recipe pages """
    
    response = APP_SESSION.get( url=URI, headers= REQUEST_HEADER )
    source = None

    if( response.status_code == 200 ):
        source = response.text
        print( Color.GREEN.value + "[+] ", end= Color.END.value  )
        print( "getting data from", URI )
    else:
        print( Color.RED.value + "[+] ", end= Color.END.value  )
        print( "HTTP request failed", URI )
    
    if( source ):
        source = Selector( text= source )
        listUrl = source.css( "a.recipe-card-link" ).xpath( '@href' ).getall()
        urlAppender( listUrl, recipeUrls )

def extractPageURL( url, paginations ):
    """ extract pages recipe URL """
    url = 'https://www.marmiton.org' + url
    response = APP_SESSION.get( url=url, headers= REQUEST_HEADER )
    source = None

    if( response.status_code == 200 ):
        source = response.text

    if( source ):
        source = Selector( text= source )
        listUrl = source.css( "nav.af-pagination > ul > li > a" ).xpath( '@href' ).getall()
        for URL in listUrl:
            if( URL != [] ):
                paginations.append( 'https://www.marmiton.org' + URL )

def getURLFromSeekWords( words, recipeUrls ):
    """ getting all urls """
    for word in words:
        URI = "https://www.marmiton.org/recettes/recherche.aspx?type=all&aqt=" + word

        response = APP_SESSION.get( url=URI, headers= REQUEST_HEADER )
        
        source = None
        if( response.status_code == 200 ):
            source = response.text
            print( Color.GREEN.value + "[+] ", end= Color.END.value  )
            print( "getting data from", URI )
        else:
            print( Color.RED.value + "[+] ", end= Color.END.value  )
            print( "HTTP request failed", URI )
        
        if( source ):
            source = Selector( text= source )
            listUrl = source.css( "a.recipe-card-link" ).xpath( '@href' ).getall()
        
        urlAppender( listUrl, recipeUrls )
        paginations = source.css( "nav.af-pagination > ul > li > a" ).xpath( '@href' ).getall()

        moreInfo = source.css( "div.showMorePages > li > a" ).xpath( '@href' ).getall()
        
        if( len( moreInfo ) > 0 ):
            for url in moreInfo:
                extractPageURL( url, paginations )

        if( paginations != [] ):
            for i, pagination in enumerate( paginations, start=1 ):
                pagination = "https://www.marmiton.org" + pagination
                if( str('page=' + str( i )) in pagination ):
                    extractURIFromPage( URI= pagination, recipeUrls= recipeUrls )

def getListWords( filename="seek_word" ):
    """ Fetch all word in the file """
    data = []

    with open( filename + '.json', 'r' ) as outfile:
        dataJson = json.load( outfile )

    for word in dataJson["seek_words"]["words"]:
        data.append( word )

    return data

def main(): 
    """ Main function """
    words = getListWords()

    urls = []
    
    APP_SESSION.trust_env = False
    APP_SESSION.get( url="https://marmiton.org", headers= REQUEST_HEADER )
    
    getURLFromSeekWords( words, urls )


    for url in urls:
        response = APP_SESSION.get( url=url, headers= REQUEST_HEADER )
        
        source = None

        if( response.status_code == 200 ):
            source = response.text
            print( Color.GREEN.value + "[+] ", end=Color.END.value )
            print( "getting data from", url )
        else:
            print( Color.RED.value + "[-] ", end=Color.END.value )
            print( "HTTP request failed", url )

        if( source ):
            selector = Selector( text= source )
            DOMParser( selector )

if __name__ == '__main__':
    main()