import {Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {KeywordRecipe} from "./keywordRecipe.entity";
import {Repository} from "typeorm";

@Injectable()
export class KeywordRecipeService {
    constructor(
        @InjectRepository(KeywordRecipe)
        private KeywordRecipeRepository : Repository<KeywordRecipe>
    ) {}
}
