import {Column, Entity, ManyToMany, PrimaryGeneratedColumn} from "typeorm";
import {Recipe} from "../recipe/recipe.entity";

@Entity()
export class KeywordRecipe {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true})
    value: string;

    @ManyToMany(type => Recipe, recipe => recipe.keywordRecipe)
    recipe: Recipe[];
}
