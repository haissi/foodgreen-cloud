import {Module} from "@nestjs/common";
import {TypeOrmModule} from "@nestjs/typeorm";
import {KeywordRecipe} from "./keywordRecipe.entity";
import {KeywordRecipeService} from "./keywordRecipe.service";

@Module({
    imports: [TypeOrmModule.forFeature([KeywordRecipe])],
    controllers: [],
    providers: [KeywordRecipeService]
})
export class KeywordRecipeModule {

}
