import {Column, Entity, OneToMany, PrimaryGeneratedColumn, ManyToMany} from "typeorm";
import {IngredientRecipe} from "../ingredientRecipe/ingredientRecipe.entity";
import {Customer} from "src/customer/customer.entity";

@Entity()
export class Ingredient {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true})
    name: string;

    @OneToMany(type => IngredientRecipe, ingredientRecipe => ingredientRecipe.ingredient)
    ingredientRecipe: IngredientRecipe[];

    @ManyToMany( type => Customer, customersAllergies => customersAllergies.allergies, { 
        primary: true,
     })
    customersAllergies : Customer[];
}
