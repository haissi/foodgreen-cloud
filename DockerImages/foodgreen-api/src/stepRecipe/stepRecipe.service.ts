import {Injectable} from "@nestjs/common";
import {StepRecipe} from "./stepRecipe.entity";
import {Repository} from "typeorm";
import {InjectRepository} from "@nestjs/typeorm";

@Injectable()
export class StepRecipeService {
    constructor(
        @InjectRepository(StepRecipe)
        private imageRecipeRepository: Repository<StepRecipe>
    ) {}
}
