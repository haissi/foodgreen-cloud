import {Module} from "@nestjs/common";
import {TypeOrmModule} from "@nestjs/typeorm";
import {StepRecipe} from "./stepRecipe.entity";
import {StepRecipeService} from "./stepRecipe.service";

@Module({
    imports: [TypeOrmModule.forFeature([StepRecipe])],
    controllers: [],
    providers: [StepRecipeService]
})
export class StepRecipeModule {

}
