import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Recipe} from "../recipe/recipe.entity";

@Entity()
export class StepRecipe {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    step : string;

    @Column({
        name: 'number_step'
    })
    numberStep : number;

    @ManyToOne(type => Recipe, recipe => recipe.stepRecipe)
    recipe : Recipe;
}
