import { Controller, Request, UseGuards, Post, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { LocalAuthGuard } from './auth/local-auth.guard';
import { AuthService } from './auth/auth.service';
import { JwtAuthGuard } from './auth/jwt-auth.guard';

@Controller('auth')
export class AppController {

	constructor(
		private readonly _appService: AppService,
		private readonly _authService: AuthService ) { }

	@UseGuards( LocalAuthGuard )
	@Post('login')
	async login( @Request() req ) {
	  	return this._authService.login( req.user );
	}
}
