import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Recipe} from "./recipe/recipe.entity";
import {RecipeModule} from "./recipe/recipe.module";
import {ImageRecipe} from "./imageRecipe/imageRecipe.entity";
import {StepRecipe} from "./stepRecipe/stepRecipe.entity";
import {KeywordRecipe} from "./keywordRecipe/keywordRecipe.entity";
import {Ingredient} from "./ingredient/ingredient.entity";
import {IngredientRecipe} from "./ingredientRecipe/ingredientRecipe.entity";
import { Customer } from './customer/customer.entity';
import { Address } from './address/address.entity';
import { CustomerModule } from './customer/customer.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
      TypeOrmModule.forRoot({
        type: 'postgres',
        host: '10.43.249.121',
        port: 5432,
        username: 'postgres',
        password: 'admin',
        database: 'food_green',
        synchronize: true,
        autoLoadEntities: true,
        keepConnectionAlive: true,
        entities: [
          Recipe,
          Address,
          Customer,
          StepRecipe,
          Ingredient,
          ImageRecipe,
          KeywordRecipe,
          IngredientRecipe,
        ],
      }),
      AuthModule,
      RecipeModule,
      CustomerModule,
  ],
  controllers: [
    AppController
  ],
  providers: [
    AppService
  ],
})

export class AppModule {


}
