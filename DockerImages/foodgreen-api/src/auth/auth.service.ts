import { Injectable } from '@nestjs/common';
import { CustomerService } from '../customer/customer.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
      private _customerService: CustomerService,
      private _jwtService: JwtService,
    ) {

  }

  async validateUser( email: string, password: string,  ): Promise<any> {
    const user = await this._customerService.findOneByEmail( email );

    if ( user && user.password === password ) {
      const { password, ...result } = user;
      return result;
    }
    
    return null;
  }

  async login( user:any ) {
    const payload = { username: user.username, sub: user.userId };
    return {
      access_token: this._jwtService.sign( payload ),
    };
  }
}