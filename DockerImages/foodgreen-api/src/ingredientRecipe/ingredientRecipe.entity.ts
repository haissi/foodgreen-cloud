import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Ingredient} from "../ingredient/ingredient.entity";
import {Recipe} from "../recipe/recipe.entity";

enum Measures {
    gr,
    kg,
    cl,
    ml,
}

@Entity()
export class IngredientRecipe {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    quantity: number;

    @Column({ nullable: true })
    measures : Measures;

    @ManyToOne(type => Ingredient, ingredient => ingredient.ingredientRecipe)
    ingredient: Ingredient;

    @ManyToOne(type => Recipe, recipe => recipe.ingredientRecipe)
    recipe: Recipe;
}
