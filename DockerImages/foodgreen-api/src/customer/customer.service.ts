import {Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import { Customer } from "./customer.entity";

@Injectable()
export class CustomerService {
    constructor(
        @InjectRepository( Customer )
        private _customerRepository: Repository<Customer>
    ) { }

    public findAll(): Promise<Customer[]> {
        return this._customerRepository.find({
            relations: [ 'allergies', 'favorites', 'address'],
        });
    }

    public async findById( id: number ): Promise<Customer> {
        return this._customerRepository.findOne( id ,{
            relations: [ 'allergies', 'favorites', 'address'],
        });
    }

    public async findOneByEmail( email: string ): Promise<Customer> {
        return this._customerRepository.createQueryBuilder('customer')
            .leftJoinAndSelect( 'customer.allergies','allergy')
            .leftJoinAndSelect( 'customer.favorites','favorites ')
            .leftJoinAndSelect( 'customer.address', 'address')
            .where( 'customer.email = :value', { value: email } )
            .getOne();
    }
}