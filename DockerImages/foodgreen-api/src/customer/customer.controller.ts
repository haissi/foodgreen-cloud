import {Controller, Get, Post, Param, Body, HttpStatus, HttpException } from "@nestjs/common";
import { CustomerService } from "./customer.service";
import { Customer } from "./customer.entity";

@Controller( 'customer' )
export class CustomerController {

    constructor(private _customerService : CustomerService ) {}

    @Get()
    findAll() : Promise<Customer[]> {
        return this._customerService.findAll();
    }

    @Get( ':id' )
    findOne( @Param() params ) : Promise<Customer> {
        return this._customerService.findById( params.id )
            .then( 
                ( datas: Customer ) => {
                    if( datas === undefined ) {
                        throw new HttpException({
                            status: HttpStatus.NOT_FOUND,
                            error: 'User not found',
                        }, HttpStatus.NOT_FOUND );
                    }
                    return datas;
                }
            );
    }

    @Post( 'findOneByEmail' )
    findOneByEmail( @Body() params ) : Promise<Customer> {
        return this._customerService.findOneByEmail( params.email )
            .then( 
                ( datas: Customer ) => {
                    console.log(datas);
                    if( datas === undefined ) {
                        throw new HttpException({
                            status: HttpStatus.NOT_FOUND,
                            error: 'User not found',
                        }, HttpStatus.NOT_FOUND );
                    }
                    return datas;
                }
            );
    }
}
