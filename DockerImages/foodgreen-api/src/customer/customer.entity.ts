import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn, DeleteDateColumn, UpdateDateColumn, OneToMany, JoinTable, ManyToOne, ManyToMany } from "typeorm";
import { Address } from "../address/address.entity";
import { Ingredient } from "src/ingredient/ingredient.entity";
import { Recipe } from "src/recipe/recipe.entity";

@Entity()
export class Customer {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstname : string;

    @Column()
    lastname : string;

    @Column({ 
        name: 'birth_date'
    })
    birthDate : Date;

    @Column({
        name: 'phone_number',
        nullable: true
    })
    phoneNumber: string;

    @Column({ unique: true })
    email : string;

    @Column()
    password : string;

    @CreateDateColumn()
    created : Date;

    @UpdateDateColumn()
    updated : Date;

    @DeleteDateColumn()
    deleted : Date;

    @ManyToOne( type => Address, address => address.id )
    address : Address[];

    @ManyToMany( type => Ingredient, ingredient => ingredient.customersAllergies, {
        primary: true,
    })
    @JoinTable({
        name: 'allergy',
    })
    allergies : Ingredient[];

    @ManyToMany( type => Recipe, recipe => recipe.customersFavorites, {
        primary: true,
    })
    @JoinTable({
        name: 'favorite',
    })
    favorites: Recipe[];
}
