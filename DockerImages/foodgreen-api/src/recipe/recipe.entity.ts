import {Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {ImageRecipe} from "../imageRecipe/imageRecipe.entity";
import {StepRecipe} from "../stepRecipe/stepRecipe.entity";
import {KeywordRecipe} from "../keywordRecipe/keywordRecipe.entity";
import {IngredientRecipe} from "../ingredientRecipe/ingredientRecipe.entity";
import { Customer } from "src/customer/customer.entity";

@Entity()
export class Recipe {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true })
    title : string;

    @Column()
    level : number;

    // cookTime only in minutes
    @Column({
        name: 'cook_time',
    })
    cookTime : number;

    // preparationTime only in minutes
    @Column({
        name: 'preparation_time',
    })
    preparationTime : number;

    @Column({
        name: 'number_people',
    })
    numberPeople : number;

    @OneToMany(type => ImageRecipe, imageRecipe => imageRecipe.recipe)
    image: ImageRecipe[];

    @OneToMany(type => StepRecipe, stepRecipe => stepRecipe.recipe)
    stepRecipe : StepRecipe[];

    @OneToMany(type => IngredientRecipe, ingredientRecipe => ingredientRecipe.recipe)
    ingredientRecipe: IngredientRecipe[];
    
    @ManyToMany(type => KeywordRecipe, keywordRecipe => keywordRecipe.recipe)
    @JoinTable({
        name: 'recipe_keyword_recipe'
    })
    keywordRecipe: KeywordRecipe[];

    @ManyToMany( type => Customer, customer => customer.favorites )
    customersFavorites: Customer[];
}
