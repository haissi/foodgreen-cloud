import {Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {Recipe} from "./recipe.entity";
import {Repository} from "typeorm";

@Injectable()
export class RecipeService {
    constructor(
        @InjectRepository(Recipe)
        private recipeRepository: Repository<Recipe>
    ) {}

    findAll(): Promise<Recipe[]> {
       return this.recipeRepository.find({relations: ['image', 'stepRecipe', 'keywordRecipe', 'ingredientRecipe', 'ingredientRecipe.ingredient']});
    }

    findOne(id: number) : Promise<Recipe> {
        return this.recipeRepository.findOne(id, { relations : ['image', 'stepRecipe', 'keywordRecipe', 'ingredientRecipe', 'ingredientRecipe.ingredient']});
    }

    findAllByKeyword(keyword : string) : Promise<Recipe[]> {
        return this.recipeRepository.createQueryBuilder('recipe')
            .leftJoinAndSelect('recipe.keywordRecipe', 'keyword_recipe')
            .leftJoinAndSelect('recipe.image', 'image_recipe')
            .where('keyword_recipe.value = :value', {value : keyword}).getMany()
    }
}
