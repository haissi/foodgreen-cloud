import {Controller, Get, Param} from "@nestjs/common";
import {Recipe} from "./recipe.entity";
import {RecipeService} from "./recipe.service";

@Controller('recipe')
export class RecipeController {

    constructor(private recipeService : RecipeService) {}

    @Get()
    findAll() : Promise<Recipe[]> {
        return this.recipeService.findAll();
    }

    @Get(':id')
    findOne(@Param() params) : Promise<Recipe> {
        return this.recipeService.findOne(params.id);
    }

    @Get('/keyword/:keyword')
    findAllByKeyword(@Param() params) : Promise<Recipe[]> {
        return this.recipeService.findAllByKeyword(params.keyword)
    }

}
