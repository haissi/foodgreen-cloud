import {Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {ImageRecipe} from "./imageRecipe.entity";
import {Repository} from "typeorm";

@Injectable()
export class ImageRecipeService {
    constructor(
        @InjectRepository(ImageRecipe)
        private imageRecipeRepository: Repository<ImageRecipe>
    ) {}
}
