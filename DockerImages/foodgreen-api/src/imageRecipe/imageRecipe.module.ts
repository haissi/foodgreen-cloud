import {Module} from "@nestjs/common";
import {TypeOrmModule} from "@nestjs/typeorm";
import {ImageRecipe} from "./imageRecipe.entity";
import {ImageRecipeService} from "./imageRecipe.service";

@Module({
    imports: [TypeOrmModule.forFeature([ImageRecipe])],
    controllers: [],
    providers: [ImageRecipeService]
})
export class ImageRecipeModule {

}
