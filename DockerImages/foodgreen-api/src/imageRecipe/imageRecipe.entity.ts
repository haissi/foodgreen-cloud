import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Recipe} from "../recipe/recipe.entity";

@Entity()
export class ImageRecipe {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true})
    url: string;

    @ManyToOne(type => Recipe, recipe => recipe.image)
    recipe: Recipe;
}
