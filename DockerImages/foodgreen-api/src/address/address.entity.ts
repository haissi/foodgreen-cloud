import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from "typeorm";
import { Customer } from "src/customer/customer.entity";

@Entity()
export class Address {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        name: 'street_name',
    })
    streetName : string;

    @Column({
        name: 'street_number',
    })
    streetNumber : string;

    @Column()
    city : string;
    
    @Column({
        name: 'zip_code',
    })
    zipCode : string
    
    @Column()
    country : string;

    @OneToMany( type => Address, address => address.id )
    customerId : Customer;

}
