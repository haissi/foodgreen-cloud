System: Ubuntu 18.04 server
Commencer par cloner le repo et s'y déplacer

# 1- Installation de rancher k3s sur le raspberry

## Rancher k3s v2.1
Lancer l'installation avec cette commande.

```console
$ curl -sfL https://get.k3s.io | sh -
```
Pour utiliser Kubectl en mode user normal il faut donner les droits au fichier:

```console
chmod 644 /etc/rancher/k3s/k3s.yaml
```
Il faut maintenant ajouter ce fichier dans le fichier .zshrc totalement en bas:

```console
$ nano ~/.zshrc
```

`export KUBECONFIG="/etc/rancher/k3s/k3s.yaml" `

Puis lancer la commande suivante pour faire valider la modification:

```console
$ source .zshrc
```

La version qui est installée est la version courante.
Or nous voulons la 2.1. Les fichiers relatives à cette version se trouve dans le répertoire rancher-2.1, il va falloir copier tous ces fichiers
dans le répertoire manifests de rancher:

```console
$ sudo cp -r manifests/ /var/lib/rancher/k3s/server/
```

Et maintenant delete tous les fichiers qui sont dedans afin qu'ils soient de nouveau pris en compte. (Tous les fichiers dans /var/lib/rancher/k3s/server/manifests
  sont automatiquement appliqués sur le cluster)

```console
$ sudo kubectl delete -f /var/lib/rancher/k3s/server/manifests/
```

Le cluster rancher k3s est maintenant disponible dans la version que nous désirons.

![Kubectl get all](all.png)

## Install Helm

```console
$ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
$ chmod 700 get_helm.sh
$ ./get_helm.sh
```

##Creer les namespaces

Depuis la raçine il faut apply tous les fichiers qui se trouvent dans le répertoire namespaces:

```console
$ kubectl apply -f namespaces/
```

##Créer les volumes persistants

Nous en aurons besoins pour que nos données soient conservées même quand les pods des applications s'éteignent. Les fichiers relatives aux voluments sont
dans le répertoire volumes:

```console
$ kubectl apply -f volumes/pvc-postgres.yaml
```
##Créer l'application postgresql

Pour créer cette application nous allons utiliser un chart helm qui a déjà été configuré. Les détails sont dans le répertoire postgresql. Il nous suffit de lancer l'Installation
avev la commande helm sur le bon namespaces:

```console
$ cd charts
$ helm install postgresql postgresql -n postgres
```
Acutellement pour accéder à la bdd:

```console
$ psql -U postgres -h 217.182.170.11 --password -p 30835 foodGreen
```
Avec:

host: 217.182.170.11
port: 30835
database: foodGreen

### Erreur rencontrée pour accéder au Pod:

```console
➜  foodgreen-cloud git:(master) ✗ kubectl exec -it pod/postgres-55b6c7fd9d-ff6pq /bin/bash -n postgres
kubectl exec [POD] [COMMAND] is DEPRECATED and will be removed in a future version. Use kubectl kubectl exec [POD] -- [COMMAND] instead.
error: unable to upgrade connection: Forbidden (user=kube-apiserver, verb=create, resource=nodes, subresource=proxy)
```

Pour fix cette erreur:

```console
➜  foodgreen-cloud git:(master) ✗ kubectl create clusterrolebinding apiserver-kubelet-admin --user=kube-apiserver --clusterrole=system:kubelet-api-admin
clusterrolebinding.rbac.authorization.k8s.io/apiserver-kubelet-admin created
```

## Créer l'application foodgreen-api

L'image a été créée à partir d'un Dockerfile disponible dans le répertoire DockerImages/foodgreen-api
Pour lancer le build:

```console
$ cd DockerImages/foodgreen-api
$ docker login registry.gitlab
$ docker build -t registry.gitlab.com/haissi/foodgreen-cloud:node-user .
$ docker push registry.gitlab.com/haissi/foodgreen-cloud:node-user
```
L'image est directement push sur le registry privé de l'équipe.

### Utiliser les images du régistry privé

Avant d'utiliser les images d'un registry privé il faut créer un secret, un imagepullsecret

```console
$ kubectl create secret generic foodgreen-registry --from-file=.dockerconfigjson=$HOME/.docker/config.json --type=kubernetes.io/dockerconfigjson -n <le namespace spécifique, ici cétait api>
$ kubectl patch serviceaccount default -p '{"imagePullSecrets": [{"name": "foodgreen-registry"}]}' -n api
```

Et voilà on peut maintenant déployer sans avoir un Errorpullbackoff.

## Déployer l'api

Un chart a été créé pour l'api, il faut se déplacer dans le répertoire charts

```console
$ cd charts
$ helm install foodgreen-api foodgreen-api -n api
```

On sait que l'api s'est connecté à la bdd quand on a ça:

![Connected ](api-connected.png)

Dans la base de données on a ça:

![Tables dans foodGreen](contentdb.png)

L'API est dispo à l'adresse api.foodgreen.fr

## Mise en place CI/CD

L'intégration continue va permettre de lancer vos tests et vos builds directement sur le serveur via des pipelines. Les pipelines sont des groupes de jobs qui vont définir les scripts à exécuter sur le serveur. Pour gérer vos pipelines, il faut mettre en place un GitLab Runner. Le GitLab Runner va gérer vos jobs et les lancer automatiquement quand une branche sera envoyée sur le dépôt ou lorsqu'elle sera mergée, par exemple. Vous pouvez également lancer les jobs à la main ou changer complètement la configuration.

Pour l'installation suivre manuelle suivre [la documentation de gitlab](https://docs.gitlab.com/runner/install/linux-manually.html) (pas conseillé).

### Connecter son cluster kubernetes à GitLab

Une autre manière d'installer le gitlab runner est de le faire en mode graphique. On va commencer par relier notre cluster à gitlab en allant dans: "Opération --> kubernetes", on choisit d'ajouter un cluster et on suit cette [documentation de gitlab](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html).

![Cluster](ClusterFoodgreen.PNG)

Une fois le cluster ajouté il faut installer l'application "Gitlab runner". Les erreurs rencontrées sont basiques et on peut trouver des résolutions en faisant des logs sur le pod qui pose problème

Notre CI/CD s'exécute lorsqu'un push est fait sur la branche master du projet foodgreen-api.
Deux jobs sont exécutés: buildImage et deploy.

### .gitlab-ci.yaml
Dans ce fichier nous avons mis tous les détails pour les jobs buildImage et deploy.
Ces jobs utilisent le gitlab-runner créé précédemment.

La partie buildImage utilise [kanico](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html). Au début j'utilisais docker mais j'avais du mal avec le job suivant.

kaniko est un outil pour construire des images docker à partir d’un Dockerfile, à l’intérieur d’un conteneur ou d’un cluster Kubernetes.

kaniko résout deux problèmes avec la méthode de construction de Docker-in-Docker :

- Docker-in-Docker nécessite un mode privilégié pour fonctionner, ce qui est un problème de sécurité important.
- Docker-in-Docker encourt généralement une pénalité de performance et peut être assez lent.

La partie build utilise une image helm. Je fais un upgrade de l'application utilisant l'image qui vient d'être créée.


Conf CI/CD compréhension:
https://learn.gitlab.com/c/why-ci-cd?x=9bMs5b&utm_medium=email&utm_source=marketo&utm_campaign=20200519GitLabVirtualConnectFrance

## Installation Sonarqube

A ce niveau si vous avez créé tous les namespaces du répertoire namespaces alors le namespace sonarqube a du être créé.

Dans le répertoire volumes nous allons apply sonar-data.yaml et sonar-extensions.yaml

```console
$ cd volumes
$ kubectl apply - sonar-data.yaml sonar-extensions.yaml
```

Ces volumes stockeront les extensions renouvelées lors des mises à jours de Sonarqube et les données de l'application.

Une fois les volumes crées nous allons déployer le chart de la base de données:

```console
$ cd charts
$ helm install sonar-data postgresql-sonar -n sonarqube
```
Nous aurons besoin de l'adresse de notre pod de base de données pour sonarqube afin qu'il se lie à l'application. L'adresse sera renseignée dans le fichier deployment.yaml de l'appli.

Une fois la base de données déployées il faut déployer l'application même:

```console
$ cd charts
$ helm install sonarqube sonarqube-chart -n sonarqube
```

Sonarqube est maintenant disponible avec les informations suivantes:

```console
Sonarqube
Addresse: sonar.foodgreen.fr
Administrator:
username: admin
mdp: QqFZn0tUK
```

You do have to restart the service if you are trying to change the perms via env variable or command line option. It rewrites the file and sets permissions on startup.
sudo chmod 644 /etc/rancher/k3s/k3s.yaml

## Monitoring

### Prométheus

On installe très facilement avec la commande:

```console
$ helm install prometheus stable/prometheus --namespace monitoring
```
En faisant un:

```console
$ kubectl get all -n monitoring
```

On a toutes les informations nécessaires.

### Grafana

Lancer la commande:

```console
$ helm install grafana grafana -n monitoring
```

On peut retrouver grafana à l'adresse: https://grafan.foodgreen.fr
Il faut ajouter la source prométheus en fonction de l'adresse donnée lors de l'installation précédente.
Et ensuite ajouter des templates de graph disponibles sur le site de grafana.

## HTTPS

Au niveau du resolver ce qui doit être ajouté c'est 'letsencrypt' uniquement, le staging ne donne pas de certificat.
Il faut attendre 10 mins avant que celui-ci soit prit en compte.
Le certificat créé une fois est sauvegarder dans un volume, il faut changer le nom du volume, ainsi tout est réinitisalisé
ATTENTION!: ne pas supprimer le http dans le ingress, c'est important.

![Deploymenttraefik](deploymenttraefik.PNG)

![pvctraefik](pvctraefik.PNG)



## Finalité

Il suffit de faire un push sur le projet foodgreen-api pour mettre à jour l'application.
